// Rapid Rag Doll


plugins {
    kotlin("jvm") version "1.9.10"
    id("org.openjfx.javafxplugin") version "0.1.0"
    application
}

repositories {
    mavenCentral()
    // My other projects hosted on gitlab.com
    maven {
        name = "tickle"
        url = uri("https://gitlab.com/api/v4/projects/9097019/packages/maven")
    }
    maven {
        name = "scarea"
        url = uri("https://gitlab.com/api/v4/projects/15723319/packages/maven")
    }
    maven {
        name = "feather"
        url = uri("https://gitlab.com/api/v4/projects/16531834/packages/maven")
    }
    maven {
        name = "paratask"
        url = uri("https://gitlab.com/api/v4/projects/9096904/packages/maven")
    }
    maven {
        name = "harbourfx"
        url = uri("https://gitlab.com/api/v4/projects/11515658/packages/maven")
    }
}

javafx {
    version = "13"
    javafx {
        modules("javafx.controls", "javafx.swing", "javafx.web")
    }
}

val rapidRagDollVersion = "0.1"
version = rapidRagDollVersion
group = "uk.co.nickthecoder"

dependencies {
    implementation("uk.co.nickthecoder:tickle-core:0.2")
    implementation("uk.co.nickthecoder:tickle-editor:0.2")
    implementation("org.reflections:reflections:0.9.11")
}

application {
    mainClass.set("uk.co.nickthecoder.rapidragdoll.RapidRagDollKt")
}

task<Exec>("publishZip") {
    dependsOn(":distZip")
    commandLine("cp", "build/distributions/rapidRagDoll-${rapidRagDollVersion}.zip", "../download/")
}

task<Exec>("ntc") {
    dependsOn(":publishZip")
    commandLine("echo", "Done")
}

defaultTasks("installDist")
